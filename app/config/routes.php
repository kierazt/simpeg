<?php
defined('BASEPATH') OR exit('No direct script access allowed');


//backend router
$route['default_controller'] = 'backend/settings/user';
$route['/'] = 'backend/settings/user/login';
$route['login'] = 'backend/settings/user/login';
$route['logout'] = 'backend/settings/user/logout';
$route['auth-user'] = 'backend/settings/user/auth';
$route['dashboard'] = 'backend/settings/user/dashboard';
$route['my-profile'] = 'backend/settings/user/my_profile';
$route['my-inbox'] = 'backend/settings/user/my_inbox';
$route['my-task'] = 'backend/settings/user/my_task';
$route['my-notif'] = 'backend/settings/user/my_notif';
$route['lock-screen'] = 'backend/settings/user/lock_screen';
$route['unlock-screen'] = 'backend/settings/user/un_lock_screen';
$route['switch-lang'] = 'backend/settings/user/switch_lang';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
