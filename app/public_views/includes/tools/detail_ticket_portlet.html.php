<div class="scroller" style="height:200px" data-always-visible="1" data-rail-visible="1" data-rail-color="blue" data-handle-color="red">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Ticket Code</label>
            <div class="input-icon right">
                <i class="fa fa-info-circle tooltips" data-original-title="Ticket Code" data-container="body"></i>
                <input class="form-control" type="text" name="code" readonly=""/> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Ticket Create Date</label>
            <div class="input-icon right">
                <i class="fa fa-info-circle tooltips" data-original-title="Ticket Code" data-container="body"></i>
                <input class="form-control" type="text" name="create_date"  readonly=""/> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Ticket Status</label>
            <div class="input-icon right">
                <i class="fa fa-info-circle tooltips" data-original-title="Ticket Code" data-container="body"></i>
                <input class="form-control" type="text" name="ticket_status"  readonly=""/> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Vendor Code</label>
            <div class="input-icon right">
                <i class="fa fa-info-circle tooltips" data-original-title="Vendor Code" data-container="body"></i>
                <input class="form-control" type="text" name="vendor_code"  readonly=""/> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Vendor Name</label>
            <div class="input-icon right">
                <i class="fa fa-info-circle tooltips" data-original-title="Vendor Name" data-container="body"></i>
                <input class="form-control" type="text" name="vendor_name"  readonly=""/> 
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <label>Issue</label>
            <textarea class="form-control" rows="3" name="content" readonly=""></textarea>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" rows="3" name="description" readonly=""></textarea>
        </div>
    </div>
</div>